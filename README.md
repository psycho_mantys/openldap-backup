![Docker Pulls](https://img.shields.io/docker/pulls/psychomantys/openldap-backup)

# openLDAP-backup

Backup OpenLDAP periodically, based on idea of [prodrigestivill/docker-postgres-backup-local](https://hub.docker.com/r/prodrigestivill/docker-postgres-backup-local/).

Supports the following Docker architectures: `linux/amd64`, `linux/arm64`, `linux/arm/v7`.

## Usage

Bind the OpenLDAP data and config directory to the container, set the envs and bind the backup directory.

Docker:

```sh
docker run -v ${LDAP_BACKUP_OUT}:/data -e LDAP_BACKUP_KEEP_DAYS=7 -e LDAP_BACKUP_KEEP_WEEKS=4 -e LDAP_BACKUP_KEEP_MONTHS=6 -e "LDAP_ADMIN_USER=${LDAP_ADMIN_USER}" -e "LDAP_ADMIN_PASSWORD=${LDAP_ADMIN_PASSWORD}" -e LDAP_BASE_DN=${LDAP_BASE_DN} -e LDAP_URI=ldap://ldap-server:389 psychomantys/openldap-backup
```

Docker Compose:

```yaml
version: '3.8'
services:
  app-ldap:
    image: osixia/openldap:latest
    environment:
      LDAP_ORGANISATION: ${LDAP_ORGANISATION}
      LDAP_BASE_DN: ${LDAP_BASE_DN}
      LDAP_ADMIN_PASSWORD: ${LDAP_ADMIN_PASSWORD}
      LDAP_CONFIG_PASSWORD: ${LDAP_CONFIG_PASSWORD}
      LDAP_DOMAIN: ${LDAP_DOMAIN}
    volumes:
      - app_ldap_data:/var/lib/ldap
      - app_ldap_config:/etc/ldap/slapd.d

  app-backup-ldap:
    image: psychomantys/openldap-backup:latest
    environment:
      LDAP_BACKUP_KEEP_DAYS: 7
      LDAP_BACKUP_KEEP_WEEKS: 4
      LDAP_BACKUP_KEEP_MONTHS: 12
      LDAP_BASE_DN: ${LDAP_BASE_DN}
      LDAP_ADMIN_USER: ${LDAP_ADMIN_USER}
      LDAP_ADMIN_PASSWORD: ${LDAP_ADMIN_PASSWORD}
      LDAP_URI: ldap://${LDAP_HOST}:${LDAP_PORT}
    volumes:
      - type: bind
        source: /home/backup/openldap/
        target: /data

volumes:
  app_ldap_data:
  app_ldap_config:

```

### Environment Variables

| env variable | Description |
|--|--|
| LDAP_BASE_DN | Base DN for make the backup |
| LDAP_ADMIN_USER | User DN to bind and make the backup |
| LDAP_ADMIN_PASSWORD | Password for bind user |
| LDAP_URI | URI to access ldap server |
| LDAP_BACKUP_DIR | Directory to save the backup at. Defaults to `/data`. |
| LDAP_BACKUP_SUFFIX | Filename suffix to save the backup. Defaults to `.ldif.xz`. |
| LDAP_BACKUP_PREFIX | Filename prefix to save the backup. Defaults to `LDAP_URI` removing `:/`. |
| LDAP_BACKUP_KEEP_DAYS | Number of daily backups to keep before removal. Defaults to `7`. |
| LDAP_BACKUP_KEEP_WEEKS | Number of weekly backups to keep before removal. Defaults to `4`. |
| LDAP_BACKUP_KEEP_MONTHS | Number of monthly backups to keep before removal. Defaults to `6`. |
| LDAP_BACKUP_HEALTHCHECK_PORT | Port listening for cron-schedule health check. Defaults to `8080`. |
| LDAP_BACKUP_SCHEDULE | [Cron-schedule](http://godoc.org/github.com/robfig/cron#hdr-Predefined_schedules) specifying the interval between postgres backups. Defaults to `@daily`. |

### Manual Backups

By default, the backup is make daily. But, you can force a backup running the script `/backup_ldap.sh`.

To force a backup now with a container created on first section, you can make:

```sh
docker exec -it "${CONTAINER}" /backup_ldap.sh
```

### Automatic Periodic Backups

You can change the `LDAP_BACKUP_SCHEDULE` environment variable in `-e LDAP_BACKUP_SCHEDULE="@daily"` to alter the default frequency. Default is `daily`.

More information about the scheduling can be found [here](http://godoc.org/github.com/robfig/cron#hdr-Predefined_schedules).

Folders `daily`, `weekly` and `monthly` are created and populated using hard links to save disk space.

## Restore examples

Some examples to restore/apply the backups.

### Restore to a remote server

Replace the `${BACKUP_FILENAME}` and `${CONTAINER}`, from the following command:

```sh
xzcat "${BACKUP_FILENAME}" | docker exec -ti "${CONTAINER}" ldapadd -H "${LDAP_URI}" -w "${LDAP_ADMIN_PASSWORD}" -D "${LDAP_ADMIN_USER}"
```

