#!/bin/bash

if [ -z "${LDAP_BACKUP_PREFIX}" ] ; then
	export LDAP_BACKUP_PREFIX=$( tr -d '/:'<<<"${LDAP_URI}" )
fi

mkdir -p "${LDAP_BACKUP_DIR}/daily/" "${LDAP_BACKUP_DIR}/weekly/" "${LDAP_BACKUP_DIR}/monthly/"

KEEP_DAYS=${LDAP_BACKUP_KEEP_DAYS}
KEEP_WEEKS=`expr $(((${LDAP_BACKUP_KEEP_WEEKS}*7)+1))`
KEEP_MONTHS=`expr $(((${LDAP_BACKUP_KEEP_MONTHS}*31)+1))`

#Initialize filename vers
#DAILY_CONFIG_FILE="${BACKUP_DIR}/daily/config-`date +%Y-%m-%d-%H:%M:%S`${BACKUP_SUFFIX}"
#WEEKLY_CONFIG_FILE="${BACKUP_DIR}/weekly/config-`date +%G-%V`${BACKUP_SUFFIX}"
#MONTHLY_CONFIG_FILE="${BACKUP_DIR}/monthly/config-`date +%Y-%m`${BACKUP_SUFFIX}"

DAILY_DATA_FILE="${LDAP_BACKUP_DIR}/daily/${LDAP_BACKUP_PREFIX}-data-`date +%Y-%m-%d-%H:%M:%S`${LDAP_BACKUP_SUFFIX}"
WEEKLY_DATA_FILE="${LDAP_BACKUP_DIR}/weekly/${LDAP_BACKUP_PREFIX}-data-`date +%G-%V`${LDAP_BACKUP_SUFFIX}"
MONTHLY_DATA_FILE="${LDAP_BACKUP_DIR}/monthly/${LDAP_BACKUP_PREFIX}-data-`date +%Y-%m`${LDAP_BACKUP_SUFFIX}"

#slapcat -n 0 | xzcat -z > "${DAILY_CONFIG_FILE}"
ldapsearch -D "${LDAP_ADMIN_USER}" -w "${LDAP_ADMIN_PASSWORD}" -b "${LDAP_BASE_DN}" -LLL -H "${LDAP_URI}" | xzcat -z > "${DAILY_DATA_FILE}"

# Hard copy config to weekly and monthly files
#ln -vf "${DAILY_CONFIG_FILE}" "${WEEKLY_CONFIG_FILE}"
#ln -vf "${DAILY_CONFIG_FILE}" "${MONTHLY_CONFIG_FILE}"

# Hard copy data to weekly and monthly files
ln -vf "${DAILY_DATA_FILE}" "${WEEKLY_DATA_FILE}"
ln -vf "${DAILY_DATA_FILE}" "${MONTHLY_DATA_FILE}"

echo "Cleaning older backups..."
find "${LDAP_BACKUP_DIR}/daily" -maxdepth 1 -mtime +${KEEP_DAYS} -name "${LDAP_BACKUP_PREFIX}-*${LDAP_BACKUP_SUFFIX}" -exec rm -rf '{}' ';'
find "${LDAP_BACKUP_DIR}/weekly" -maxdepth 1 -mtime +${KEEP_WEEKS} -name "${LDAP_BACKUP_PREFIX}-*${LDAP_BACKUP_SUFFIX}" -exec rm -rf '{}' ';'
find "${LDAP_BACKUP_DIR}/monthly" -maxdepth 1 -mtime +${KEEP_MONTHS} -name "${LDAP_BACKUP_PREFIX}-*${LDAP_BACKUP_SUFFIX}" -exec rm -rf '{}' ';'

