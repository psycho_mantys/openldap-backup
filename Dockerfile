FROM alpine

ARG GOCRONVER=v0.0.9
ARG TARGETOS=linux
ARG TARGETARCH=amd64

#&& apk add --no-cache bash ca-certificates curl openldap xz openldap-back-mdb openldap-clients \

ENV LDAP_BACKUP_KEEP_DAYS=7 \
  LDAP_BACKUP_KEEP_WEEKS=4 \
  LDAP_BACKUP_KEEP_MONTHS=6 \
  LDAP_BACKUP_SCHEDULE="@daily" \
  LDAP_BACKUP_DIR=/data \
  LDAP_BACKUP_SUFFIX=.ldif.xz \
  LDAP_BACKUP_HEALTHCHECK_PORT=8080

RUN set -x \
	&& apk add --no-cache bash ca-certificates curl xz openldap-clients \
	&& curl -L https://github.com/prodrigestivill/go-cron/releases/download/${GOCRONVER}/go-cron-${TARGETOS}-${TARGETARCH}-static.gz | zcat > /usr/local/bin/go-cron \
	&& chmod a+x /usr/local/bin/go-cron \
	&& apk del ca-certificates && mkdir /data 

COPY backup_ldap.sh docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["start"]

HEALTHCHECK --interval=5m --timeout=3s \
  CMD /docker-entrypoint.sh healthcheck

