#!/bin/bash

set -e

parse_env(){
	if [ -r "$1" ] ; then
		while IFS="=" read key value  ; do
			export "${key}=${value}"
		done<<<"$( egrep '^[^#]+=.*' "$1" )"
	fi
}

parse_env '/env.sh'
parse_env '/run/secrets/env.sh'

if [ "start" = "$1" ]; then
	exec /usr/local/bin/go-cron -s "${LDAP_BACKUP_SCHEDULE}" -p "${LDAP_BACKUP_HEALTHCHECK_PORT}" -- /backup_ldap.sh
elif [ "healthcheck" == "$1" ]; then
	curl -f "http://127.0.0.1:${LDAP_BACKUP_HEALTHCHECK_PORT}/" || exit 1
	exit 0
fi

exec "$@"

